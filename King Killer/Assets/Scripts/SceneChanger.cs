using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public int destScene;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("CHANGE");
        SceneManager.LoadScene(destScene);
    }
    

}