using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTrigger : MonoBehaviour
{

    public GameObject textObject;

    void Awake()
    {
        textObject.SetActive(false);

    }

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Printing");
        textObject.SetActive(true);
        StartCoroutine("CountDown");
    }

    IEnumerator CountDown()
    {
        yield return new WaitForSeconds(5);
        Destroy(textObject);
        Destroy(gameObject);
    }

}
