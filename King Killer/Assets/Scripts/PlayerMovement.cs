using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // THIS SCRIPT SHOULD BE USED WITH CHARACTER CONTROLLER 2D SCRIPT ON THE SAME OBJECT

    public CharacterController2D controller;

    float horizontalMove = 0f;

    public float MoveSpeed = 10f;

    bool jump = false;

    private void Update()
    {
        horizontalMove  = Input.GetAxisRaw("Horizontal") * MoveSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;

        }
    }

    private void FixedUpdate()
    {
        controller.Move(horizontalMove, false, jump);
        jump = false;
    }
}
