﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class CamControler : MonoBehaviour
    {
        [SerializeField]
        private Transform targetToFollow;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            // camera tracks player's position and follows if Player is moving and set the Camera boundaries limit of x and Y to desired values

            transform.position = new Vector3(
                 Mathf.Clamp(targetToFollow.position.x, 0f, 100000f),
                 Mathf.Clamp(targetToFollow.position.y, 0f, 10000000f),
                 transform.position.z);

        }
    }
}